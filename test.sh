#!/bin/bash

rm tracefile.txt

traceroute -n $1 | sed s/"*"//g | grep -v traceroute | awk '{print $2}' | grep -v -e '^$'  > tracefile.txt

rm troutput.txt

while read p; do
  curl -s http://geoip.nekudo.com/api/$p >> troutput.txt
echo >> troutput.txt
done <tracefile.txt

awk -F ":" '{print $6, $7, $8}'  troutput.txt | awk -F '"' '{print $3, $5}' | sed 's/,//g'

#        for i in $( traceroute -n $1 ); do

# echo $i          
# curl http://geoip.nekudo.com/api/$i ;
        
#done
